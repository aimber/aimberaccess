### Aimber license bot

This project is used for the licensing agreement between to parties. We use this gitlab bot to automatically close merge request when the developer did not agree to the terms. The developer can then click on the link to the nodejs app and accept the agreement.

# Getting started

### Basic
1. Install latest git + nodejs 
2. Download AimberAccess

``` bash
# HTTPS
git clone https://gitlab.com/aimber/aimberaccess.git
```

3. Install dependencies

``` bash
npm install
```

3. Go to your [profile settings -> Applications](https://gitlab.com/profile/applications) and generate a new application  with scopes api and read_user. Insert your Redirect URI (http://yoururl:3000/oauth/callback).
4. Go to your [profile settings -> Personal Access Tokens ](https://gitlab.com/profile/personal_access_tokens) and generate a new  access token with scopes api and read_user. 
5. Insert your newly created token and secrets into the config.json. *DO NOT SHARE THESE INFORMATIONs WITH ANYONE ELSE*
6. Start the nodejs server via 


``` bash
node .
```

7. Optional: Create a systemd service:
``` bash
[Unit]
Description=screenplay access bot
After=system.slice multi-user.target mongod.service

[Service]
Type=forking
User=aimberaccess.screen-play

StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=nodaimberaccessebb

WorkingDirectory=/home/screen-play/homes/aimberaccess/aimberaccess
ExecStart=/usr/bin/env node .
Restart=always

```