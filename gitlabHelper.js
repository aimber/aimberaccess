var database = require('./db.js');
var config = require('./config.json');

//var Gitlab = require('gitlab');

// ES5, assuming Promise and Object.assign are available
const Gitlab = require('gitlab/dist/es5').default


// Instantiating
const api = new Gitlab({
  url:   config.gitlab_url, // Defaults to https://gitlab.com
  token: config.gitlab_api.access_token  // Can be created in your profile.
})

function isMergeAllowedForUser(username){
    var dbUser = database.selectUserByUsername(username);
    if(dbUser != null){
        if(dbUser.agreement_accepted == 1){
            console.log(`Merge allowed for user ${username}`);
            return true;
        } else {
            console.log(`Merge not allowed, user ${username} did not accept the agreement`);    
        }
    }else{
        console.log(`Merge not allowed, user ${username} not found in database`);
    }
    return false;
}

function createAgreementNotice(projectID, mergerequestID){
    var payload = {};
    payload.body = config.application.agreement_notice;
    api.MergeRequestNotes.create(projectID,mergerequestID,payload);
}

function updateMergeRequest(projectID, mergeRequestID, state_event){
    var payload = {};
    payload.state_event = state_event;
    api.MergeRequests.edit(projectID,mergeRequestID,payload);
}

function closeMergeRequest(projectID, mergeRequestID){
    updateMergeRequest(projectID,mergeRequestID,"close");
}

function reopenMergeRequest(projectID, mergeRequestID){
    updateMergeRequest(projectID,mergeRequestID,"reopen");
}

module.exports = {
    isMergeAllowedForUser,
    createAgreementNotice,
    closeMergeRequest,
    reopenMergeRequest
}