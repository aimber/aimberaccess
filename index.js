const express = require('express');
var passport = require("passport");
var session = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config.json');
var database = require('./db.js');
var gitlab = require('./gitlabHelper.js');
var fs = require('fs');
var https = require("https");

database.initDB();

const app = express();
const port = config.application.port;
const httpsPort = config.application.https_port;

const httpsConfig = {
  key: fs.readFileSync(config.application.ssl_key),
  cert: fs.readFileSync(config.application.ssl_cert)
}

app.use(express.static('public'));
app.use(bodyParser.json());

app.use(cookieParser());
app.use(session({
  secret: 'clinksecret', // TODO: STORE outside
  saveUninitialized: true,
  resave: true
}));

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

var GitlabStrategy = require('passport-gitlab2').Strategy;

passport.use(new GitlabStrategy({
  clientID: config.oauth.clientID,
  clientSecret: config.oauth.clientSecret,
  callbackURL: `${config.application.domain}:${httpsPort}/oauth/callback`,
  baseURL: config.gitlab_url
},
  function (accessToken, refreshToken, profile, cb) {
    console.log(`Passport done accesstoken : ${accessToken} , refresh token : ${refreshToken} , profile :`)
    console.log(profile)
    return cb(null, profile);
  }
));

app.get('/account/profile', async function (req, res) {
  console.log("serving /account/profile")
  if(req.user == null){
    res.redirect("/account/login?reason=invalidsession");
  }else{
    console.log(JSON.stringify(req.user));
    console.log("");
    var dbUser = database.selectUserByEmail(req.user.emails[0].value);
    if (dbUser == null) {
      database.insertUser(
        req.user.id,
        req.user.emails[0].value, 
        req.user.username, 
        req.user.displayName);
    }
    
    res.send(JSON.stringify(req.user));
    
  }
});


app.use('/account/login', express.static(__dirname + '/public/login.html'));

app.get('/account/logout', function (req, res) {
  req.session.destroy();
  res.redirect('/');
});

app.get('/oauth/gitlab', passport.authenticate('gitlab'));

app.get('/oauth/callback', passport.authenticate('gitlab', {
  successRedirect: '/account/profile',
  failureRedirect: '/account/login?reason=failedlogin'
}));

app.post('/api/merge_request', function(req, res) {
  console.log(req.body);
  var pUsername = req.body.user.username.toLowerCase();
  if(pUsername !== config.application.bot_user_name.toLowerCase())
  {
    var isAllowed = gitlab.isMergeAllowedForUser(pUsername);
    if(!isAllowed){
      var projectID = req.body.project.id;
      var mergerequestID = req.body.object_attributes.iid;
      gitlab.createAgreementNotice(projectID,mergerequestID);
      gitlab.closeMergeRequest(projectID,mergerequestID);
    }
  }
});

app.listen(port, () => {});

https.createServer(httpsConfig, app).listen(httpsPort);

console.log(`App listening on port ${httpsPort}!`);